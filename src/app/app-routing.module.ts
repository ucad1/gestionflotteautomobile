import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdmindashbordComponent } from './admin/admindashbord/admindashbord.component';
import { GestionreservationComponent } from './admin/gestionreservation/gestionreservation.component';
import { AjoutmissionComponent } from './admin/Mission/ajoutmission/ajoutmission.component';
import { ListemissionComponent } from './admin/Mission/listemission/listemission.component';
import { StatistiquesComponent } from './admin/statistiques/statistiques.component';
import { LoginComponent } from './Auth/login/login.component';
import { HomeclientComponent } from './client/homeclient/homeclient.component';
import { SigninComponent } from './client/signin/signin.component';
import { DeclareraccidentComponent } from './DChauffeurs/Alertes/declareraccident/declareraccident.component';
import { DeclareramendeComponent } from './DChauffeurs/Alertes/declareramende/declareramende.component';
import { DeclarerpannesComponent } from './DChauffeurs/Alertes/declarerpannes/declarerpannes.component';
import { DemandeabsenceComponent } from './DChauffeurs/Alertes/demandeabsence/demandeabsence.component';
import { DashboardchauffeurComponent } from './DChauffeurs/dashboardchauffeur/dashboardchauffeur.component';
import { VoirmissionsComponent } from './DChauffeurs/Missions/voirmissions/voirmissions.component';
import { DemandesabscencesComponent } from './DMecanicien/Absences/demandesabscences/demandesabscences.component';
import { ListdemandesabscencesComponent } from './DMecanicien/Absences/listdemandesabscences/listdemandesabscences.component';
import { DashboardmecanoComponent } from './DMecanicien/dashboardmecano/dashboardmecano.component';
import { ReparermaintenanceComponent } from './DMecanicien/Reparation/reparermaintenance/reparermaintenance.component';
import { ReparerpannesComponent } from './DMecanicien/Reparation/reparerpannes/reparerpannes.component';
import { AcceuilComponent } from './Home/acceuil/acceuil.component';
import { ImagecomponentComponent } from './imagecomponent/imagecomponent.component';
import { AjoutmarquesComponent } from './marques/ajoutmarques/ajoutmarques.component';
import { ListemarquesComponent } from './marques/listemarques/listemarques.component';
import { AjoutchauffeurComponent } from './Personnels/Chaufeur/ajoutchauffeur/ajoutchauffeur.component';
import { ListechauffeurComponent } from './Personnels/Chaufeur/listechauffeur/listechauffeur.component';
import { ModifierchauffeurComponent } from './Personnels/Chaufeur/modifierchauffeur/modifierchauffeur.component';
import { AjoutMecanoComponent } from './Personnels/Mecanicien/ajout-mecano/ajout-mecano.component';
import { ListemecanoComponent } from './Personnels/Mecanicien/listemecano/listemecano.component';
import { ModifierMecanoComponent } from './Personnels/Mecanicien/modifier-mecano/modifier-mecano.component';
import { UploadimgComponent } from './uploadimg/uploadimg.component';
import { AjoutvehiculeComponent } from './vehicule/ajoutvehicule/ajoutvehicule.component';
import { ListvehiculeComponent } from './vehicule/listvehicule/listvehicule.component';
import { ModifierVehiculeComponent } from './vehicule/modifier-vehicule/modifier-vehicule.component';

const routes: Routes =[
  {
    path : 'admin',
    component : AdmindashbordComponent
  },

  {
    path : 'upload',
    component : UploadimgComponent
  },
  {
    path : 'signin',
    component : SigninComponent
  },
  {
    path: 'home',
    component: AcceuilComponent,
    children : [
     { path: 'admin',
      component: AdmindashbordComponent,}
    ]

  }, {
    path: 'homeclient',
    component: HomeclientComponent,
    
  },
  {
    path: 'mecanodash',
    component: DashboardmecanoComponent,
    children: [
      {
        path: 'maintenance',
        component: ReparermaintenanceComponent
      },
      {
        path: 'reparerpanne',
        component: ReparerpannesComponent
      },
      {
        path: 'dabsencemecano',
        component: DemandesabscencesComponent
      },
      {
        path: 'listdabsencemecano',
        component: ListdemandesabscencesComponent
      }
    ]
  },

  {
    path: 'chauffeurdash',
    component: DashboardchauffeurComponent,
    children: [
      {
        path: 'declareramende',
     component: DeclareramendeComponent,
     }, {
      path: 'declareraccident',
   component: DeclareraccidentComponent,
   }, {
    path: 'declarerpannes',
 component: DeclarerpannesComponent,
 }, {
  path: 'dabsence',
component: DemandeabsenceComponent,
}, {
  path: 'voirmission',
component: VoirmissionsComponent,
},
    ]
  },
  {
    path: 'admin',
    component: AdmindashbordComponent,
    children: [

      {
         path: 'statistiques',
      component: StatistiquesComponent,
      },
      {
        path: 'listmarque',
     component: ListemarquesComponent,
     },
     {
      path: 'ajoutmarque',
   component: AjoutmarquesComponent,
   },
   {
    path: 'ajoutvehicule',
 component: AjoutvehiculeComponent,
 },
 {
  path: 'listevehicule',
component: ListvehiculeComponent,
},
{
  path: 'ajoutchauffeur',
component: AjoutchauffeurComponent,
},
{
  path: 'ajoutimage',
 component: ImagecomponentComponent,
},
{
  path: 'listechauffeur',
component: ListechauffeurComponent,
},
{
  path: 'listemecano',
component: ListemecanoComponent,
},
{
  path: 'ajoutmecano',
component: AjoutMecanoComponent,
},
{
  path: 'gestionreservation',
  component : GestionreservationComponent,
},
{
  path: 'ajoutmission',
  component : AjoutmissionComponent,
},
{
  path: 'listemission',
  component : ListemissionComponent,
},
 {
  path: 'modifiervehicule',
  component: ModifierVehiculeComponent
 },
 {
  path : 'modifchauffeur',
  component: ModifierchauffeurComponent
 },
 {
  path: 'modifermecano',
  component: ModifierMecanoComponent
 }
     
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
