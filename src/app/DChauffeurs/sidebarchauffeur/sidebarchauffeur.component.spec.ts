import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarchauffeurComponent } from './sidebarchauffeur.component';

describe('SidebarchauffeurComponent', () => {
  let component: SidebarchauffeurComponent;
  let fixture: ComponentFixture<SidebarchauffeurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarchauffeurComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SidebarchauffeurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
