import { Component, OnInit } from '@angular/core';
import { Vehicule } from 'src/app/Model/vehicule';
import { VehiculeserviceService } from 'src/app/services/vehicule/vehiculeservice.service';

@Component({
  selector: 'app-declarerpannes',
  templateUrl: './declarerpannes.component.html',
  styleUrls: ['./declarerpannes.component.scss']
})
export class DeclarerpannesComponent implements OnInit {

  MatriculeList !: Vehicule[];
  constructor(public vehiculeservice: VehiculeserviceService) { }

  ngOnInit(): void {

    this.vehiculeservice.getAllVehicules().subscribe(
      response =>{
        this.MatriculeList= response
      }
    )
  }

}
