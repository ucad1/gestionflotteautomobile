import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeclarerpannesComponent } from './declarerpannes.component';

describe('DeclarerpannesComponent', () => {
  let component: DeclarerpannesComponent;
  let fixture: ComponentFixture<DeclarerpannesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeclarerpannesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DeclarerpannesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
