import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeclareraccidentComponent } from './declareraccident.component';

describe('DeclareraccidentComponent', () => {
  let component: DeclareraccidentComponent;
  let fixture: ComponentFixture<DeclareraccidentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeclareraccidentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DeclareraccidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
