import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeclareramendeComponent } from './declareramende.component';

describe('DeclareramendeComponent', () => {
  let component: DeclareramendeComponent;
  let fixture: ComponentFixture<DeclareramendeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeclareramendeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DeclareramendeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
