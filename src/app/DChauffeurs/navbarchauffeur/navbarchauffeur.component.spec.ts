import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarchauffeurComponent } from './navbarchauffeur.component';

describe('NavbarchauffeurComponent', () => {
  let component: NavbarchauffeurComponent;
  let fixture: ComponentFixture<NavbarchauffeurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarchauffeurComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NavbarchauffeurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
