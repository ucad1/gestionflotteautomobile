import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbarchauffeur',
  templateUrl: './navbarchauffeur.component.html',
  styleUrls: ['./navbarchauffeur.component.scss']
})
export class NavbarchauffeurComponent implements OnInit {

  nom!: any;
  prenom!: any;
  constructor(public route : Router) { }

  ngOnInit(): void {

    this.nom = localStorage.getItem('nom')
    this.prenom = localStorage.getItem('prenom')
  }


  logout(){
    localStorage.clear();
    this.route.navigate(['/home']);
  }

}
