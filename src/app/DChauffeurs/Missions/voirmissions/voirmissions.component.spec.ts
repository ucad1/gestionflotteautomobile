import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VoirmissionsComponent } from './voirmissions.component';

describe('VoirmissionsComponent', () => {
  let component: VoirmissionsComponent;
  let fixture: ComponentFixture<VoirmissionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VoirmissionsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VoirmissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
