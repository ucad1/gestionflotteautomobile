import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardchauffeurComponent } from './dashboardchauffeur.component';

describe('DashboardchauffeurComponent', () => {
  let component: DashboardchauffeurComponent;
  let fixture: ComponentFixture<DashboardchauffeurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardchauffeurComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardchauffeurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
