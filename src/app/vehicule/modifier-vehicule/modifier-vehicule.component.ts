import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Marque } from 'src/app/Model/marque';
import { MarqueserviceService } from 'src/app/services/marque/marqueservice.service';
import { VehiculeserviceService } from 'src/app/services/vehicule/vehiculeservice.service';

@Component({
  selector: 'app-modifier-vehicule',
  templateUrl: './modifier-vehicule.component.html',
  styleUrls: ['./modifier-vehicule.component.scss']
})
export class ModifierVehiculeComponent implements OnInit {

  
  MarqueList !: Marque[];
  
  constructor(private vehiculeservice : VehiculeserviceService, public marqueService: MarqueserviceService, 
    private _formBuilder: FormBuilder, private route : Router) { }
 

  ngOnInit(): void {
    
    this.marqueService.getAll().subscribe(
      response => {this.MarqueList = response}
    )
  }

}
