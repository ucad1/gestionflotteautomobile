import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { FloatLabelType } from '@angular/material/form-field';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ImageModel } from 'src/app/Model/filemodel';
import { Marque } from 'src/app/Model/marque';
import { Vehicule } from 'src/app/Model/vehicule';
import { MarqueserviceService } from 'src/app/services/marque/marqueservice.service';
import { VehiculeserviceService } from 'src/app/services/vehicule/vehiculeservice.service';

@Component({
  selector: 'app-ajoutvehicule',
  templateUrl: './ajoutvehicule.component.html',
  styleUrls: ['./ajoutvehicule.component.scss']
})
export class AjoutvehiculeComponent implements OnInit {

  public imagePath: any;
  imgURL: any;

  MarqueList !: Marque[];

  uploadedImage!: File;
  dbImage: any;
  postResponse: any;
  successResponse!: string;
  image: any;

 /* vehicule : Vehicule = {
    matricule: "",
    marque: "",
    numerosasi: "",
    designation: "",
    numcartegrise: "",
    typecarburant: "",
    puissance: 0,
    nombreplaces: 0,
    model: "",
    anneemodel: 0,
    tarifs: 0,
    dateAchat: "",
    etatVehicule: "",
    vehiculeImage: []
  };*/
  hideRequiredControl = new FormControl(false);
  floatLabelControl = new FormControl('auto' as FloatLabelType);
  options = this._formBuilder.group({
    hideRequired: this.hideRequiredControl,
    floatLabel: this.floatLabelControl,
  });
  userFile: any;
  constructor(public vehiculeservice: VehiculeserviceService, private santisizer : DomSanitizer,
    public marqueService: MarqueserviceService,private httpClient: HttpClient, private _formBuilder: FormBuilder, private route : Router) { }

  ngOnInit(): void {
    this.infoform();

    this.marqueService.getAll().subscribe(
      response => {this.MarqueList = response}
    )

  }


  infoform() {
    this.vehiculeservice.formDatas = this._formBuilder.group({
      id : null,
      matricule : ['' , Validators.required] ,
      marque : ['' , Validators.required] ,  
      numerosasi : ['' , Validators.required] ,
      numcartegrise : ['' , Validators.required] ,
      dateAchat : ['' , Validators.required] ,
      designation : ['' , Validators.required] ,
      tarifs  : ['' , Validators.required] ,
      model : ['' , Validators.required] ,
      anneemodel : ['' , Validators.required] ,
      nombreplaces  : ['' , Validators.required] ,
      puissance : ['' , Validators.required] ,
      typecarburant : ['' , Validators.required] ,
      etatVehicule : ['Disponible' , Validators.required],
      image : ['', Validators.required]

    })
  }
  getFloatLabelValue(): FloatLabelType {
    return this.floatLabelControl.value || 'auto';
  }

  resetForm() {
    this.vehiculeservice.formDatas.reset()
  }


  //on select file
  onSelectFile(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.userFile = file;
      // this.f['profile'].setValue(file);

      var mimeType = event.target.files[0].type;
      if (mimeType.match(/image\/*/) == null) {
       // this.toastr.success('Only images are supported.');

       alert('Only images are supported.');
        return;
      }
      var reader = new FileReader();
      this.imagePath = file;
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        this.imgURL = reader.result;
      }
    }
  }


  
  imageUploadAction() {
    const imageFormData = new FormData();
    imageFormData.append('image', this.uploadedImage, this.uploadedImage.name);


    this.httpClient.post('http://localhost:8089/senflotteautomobile/postvehicule/', imageFormData, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          this.postResponse = response;
          this.successResponse = this.postResponse.body.message;
        } else {
          this.successResponse = 'Image not uploaded due to some error!';
          alert(this.successResponse)
        }
      }
      );
    }


  

}
