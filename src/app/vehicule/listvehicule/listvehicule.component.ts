import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Vehicule } from 'src/app/Model/vehicule';
import { VehiculeserviceService } from 'src/app/services/vehicule/vehiculeservice.service';
import { ModifierVehiculeComponent } from '../modifier-vehicule/modifier-vehicule.component';

@Component({
  selector: 'app-listvehicule',
  templateUrl: './listvehicule.component.html',
  styleUrls: ['./listvehicule.component.scss']
})
export class ListvehiculeComponent implements OnInit {

  vehicules !: Vehicule[];

  displayedColumns: string[] = ['id','matricule','numerosasi','numcartegrise','nombreplaces','tarifs','typecarburant','etatVehicule','action'];
  dataSource!: MatTableDataSource<Vehicule>;

  @ViewChild(MatPaginator) paginator!: MatPaginator ;
  @ViewChild(MatSort) sort!: MatSort;

  
  constructor(public vehiculeservice : VehiculeserviceService,public dialog: MatDialog,  public route : Router) { }

  ngOnInit(): void {

    this.getVehicules();
  }

  updtaeVehiculeDialog() {
   this.route.navigate(['/modifiervehicule'])
  }
  getVehicules() {
    this.vehiculeservice.getAllVehicules().subscribe({
      next: (res)=>{
       
        this.dataSource = new MatTableDataSource(res); 
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error : (err : HttpErrorResponse)=> {
        err.message
      }
    })
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
