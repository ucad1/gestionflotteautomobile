import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardmecanoComponent } from './dashboardmecano.component';

describe('DashboardmecanoComponent', () => {
  let component: DashboardmecanoComponent;
  let fixture: ComponentFixture<DashboardmecanoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardmecanoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DashboardmecanoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
