import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandesabscencesComponent } from './demandesabscences.component';

describe('DemandesabscencesComponent', () => {
  let component: DemandesabscencesComponent;
  let fixture: ComponentFixture<DemandesabscencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemandesabscencesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DemandesabscencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
