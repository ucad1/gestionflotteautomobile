import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListdemandesabscencesComponent } from './listdemandesabscences.component';

describe('ListdemandesabscencesComponent', () => {
  let component: ListdemandesabscencesComponent;
  let fixture: ComponentFixture<ListdemandesabscencesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListdemandesabscencesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListdemandesabscencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
