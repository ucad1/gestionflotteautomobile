import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbarmecano',
  templateUrl: './navbarmecano.component.html',
  styleUrls: ['./navbarmecano.component.scss']
})
export class NavbarmecanoComponent implements OnInit {

  nom!: any;
  prenom!: any;

  constructor(public route : Router) { }

  ngOnInit(): void {
    this.nom = localStorage.getItem('nom')
    this.prenom = localStorage.getItem('prenom')
  }

  logout(){
    localStorage.clear();
    this.route.navigate(['/home'])
  }

}
