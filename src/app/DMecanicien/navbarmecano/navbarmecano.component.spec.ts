import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarmecanoComponent } from './navbarmecano.component';

describe('NavbarmecanoComponent', () => {
  let component: NavbarmecanoComponent;
  let fixture: ComponentFixture<NavbarmecanoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarmecanoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NavbarmecanoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
