import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarmecanoComponent } from './sidebarmecano.component';

describe('SidebarmecanoComponent', () => {
  let component: SidebarmecanoComponent;
  let fixture: ComponentFixture<SidebarmecanoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarmecanoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SidebarmecanoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
