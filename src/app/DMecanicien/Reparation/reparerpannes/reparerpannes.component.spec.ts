import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReparerpannesComponent } from './reparerpannes.component';

describe('ReparerpannesComponent', () => {
  let component: ReparerpannesComponent;
  let fixture: ComponentFixture<ReparerpannesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReparerpannesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReparerpannesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
