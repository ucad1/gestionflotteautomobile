import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReparermaintenanceComponent } from './reparermaintenance.component';

describe('ReparermaintenanceComponent', () => {
  let component: ReparermaintenanceComponent;
  let fixture: ComponentFixture<ReparermaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReparermaintenanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReparermaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
