import { ImageModel } from "./filemodel";

export interface Vehicule {
    //id : number;
	
	matricule: String ;

	marque : String;
	
	numerosasi : String ;
	
	designation: String ;
	
	numcartegrise : String ;
	
	typecarburant : String ;
	
	puissance : number  ;
	
	nombreplaces: number ;
	
	//kilometreparcourus: number ;
	
	model : String ;
	
	anneemodel : number;
	
	tarifs : number;
	
	dateAchat : String ;
	
	etatVehicule : String ;
	
	image : File;

	type: String;

	name: String;
}