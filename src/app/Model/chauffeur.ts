export interface Chauffeur {

    id: number;
    nom: String;
    prenom : String;
    numeropermis : String;
    typepermis : String;
    datedelivrance : String;
    dateexpiration : String;
    mail : String;
    numtel : String;
    password : String;
    address : String;
    photo : String;
    status : String;
}