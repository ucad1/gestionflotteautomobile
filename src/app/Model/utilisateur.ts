export interface Utilisateur {
    iduser: number;
    nom: String;
    prenom : String;
    adresse : String;
    mail : String;
    password : String;
    photo : String;
}