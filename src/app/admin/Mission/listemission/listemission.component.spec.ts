import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListemissionComponent } from './listemission.component';

describe('ListemissionComponent', () => {
  let component: ListemissionComponent;
  let fixture: ComponentFixture<ListemissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListemissionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListemissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
