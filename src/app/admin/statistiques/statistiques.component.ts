import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Vehicule } from 'src/app/Model/vehicule';
import { ChauffeurserviceService } from 'src/app/services/chauffeur/chauffeurservice.service';
import { ClientserviceService } from 'src/app/services/client/clientservice.service';
import { MarqueserviceService } from 'src/app/services/marque/marqueservice.service';
import { VehiculeserviceService } from 'src/app/services/vehicule/vehiculeservice.service';

@Component({
  selector: 'app-statistiques',
  templateUrl: './statistiques.component.html',
  styleUrls: ['./statistiques.component.scss']
})
export class StatistiquesComponent implements OnInit {

ttalChauffeur !: number;
ttalVehicule !: number;
ttalMarque !: number;
ttclient !: number;

  constructor(public chauffeurservice : ChauffeurserviceService, public vehiculeservice: VehiculeserviceService,
    public marqueservice : MarqueserviceService,public clientservice: ClientserviceService ) { }

  ngOnInit(): void {

    this.totalChauffeurs(),
    this.totalvehicule(),
    this.totalMarque(),
    this.totalClient()
  }

  totalClient(){
    this.clientservice.totalclient().subscribe(
      
      response =>{
        this.ttclient = response;
      
    })
  }
  totalMarque() {
    this.marqueservice.totalmarques().subscribe(
      
      response =>{
        this.ttalMarque = response;
      
    })
  }


  totalvehicule() {
    this.vehiculeservice.totalvehicule().subscribe(
      response =>{
       this.ttalVehicule = response;
    })
    }

  totalChauffeurs() {
  this.chauffeurservice.totalchauffeur().subscribe(
    response=>{
      this.ttalChauffeur =response;
   
  })
  }

}
