import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotusedComponent } from './notused.component';

describe('NotusedComponent', () => {
  let component: NotusedComponent;
  let fixture: ComponentFixture<NotusedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotusedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NotusedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
