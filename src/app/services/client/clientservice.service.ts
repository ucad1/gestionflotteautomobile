import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientserviceService {

  public formDatas !:  FormGroup;
  
  private apiServerUrl = environment.apibaseUrl;

  constructor(private http: HttpClient) { }

  public totalclient(): Observable<number> {
    return this.http.get<number>(`${this.apiServerUrl}/totalclient`);
  }
    //inscription
    public inscrireclient(info: Object): Observable<Object> {
      return this.http.post(`${this.apiServerUrl}/addclient`, info);
    }

    findByidusercli(iduser :any ) {
      return this.http.get(`${this.apiServerUrl}/clientss/${iduser}`);
     
     } 
}
