import { TestBed } from '@angular/core/testing';

import { MecanicienserviceService } from './mecanicienservice.service';

describe('MecanicienserviceService', () => {
  let service: MecanicienserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MecanicienserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
