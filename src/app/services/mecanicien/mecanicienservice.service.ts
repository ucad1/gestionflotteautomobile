import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Mecanicien } from 'src/app/Model/mecanicien';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MecanicienserviceService {
  private apiServerUrl = environment.apibaseUrl;


  public formdata !: FormGroup;
  
  constructor(private http: HttpClient) { }


  public getAllMecano(): Observable<Mecanicien[]> {
    return this.http.get<Mecanicien[]>(`${this.apiServerUrl}/mecaniciens`);
  }

   //ajout mecano
   public ajoutMecano(info: Object): Observable<Object> {
    return this.http.post(`${this.apiServerUrl}/addMecano`, info);
  }

  //
  findByidusermec(iduser :any ) {
    return this.http.get(`${this.apiServerUrl}/mecanoss/${iduser}`);
   
   } 
}
