import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Vehicule } from 'src/app/Model/vehicule';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VehiculeserviceService {

  public formDatas !:  FormGroup; 
  
  private apiServerUrl = environment.apibaseUrl;

  constructor(private http: HttpClient) { }


    // nombre vehicules
 
    public totalvehicule(): Observable<number> {
      return this.http.get<number>(`${this.apiServerUrl}/totalvehicules`);
    }

  public getAllVehicules(): Observable<Vehicule[]> {
    return this.http.get<Vehicule[]>(`${this.apiServerUrl}/vehicules`);
  }

  //ajout vehicule
  public ajoutVehicule(info: FormData): Observable<Vehicule> {
    return this.http.post<Vehicule>(`${this.apiServerUrl}/addvehicule`, info);
  }

  uploadFile(file: File): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', '<Server URL of the file upload>', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }
  
}
