import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Marque } from '../../Model/marque';




@Injectable({
  providedIn: 'root'
})
export class MarqueserviceService {

  public formDatas !:  FormGroup; 

  
  private apiServerUrl = environment.apibaseUrl;


  constructor(private http: HttpClient) { }

   // nombre marques
 
   public totalmarques(): Observable<any> {
    return this.http.get(`${this.apiServerUrl}/totalmarques`);
  }

  public getAll(): Observable<Marque[]> {
    return this.http.get<Marque[]>(`${this.apiServerUrl}/marques`);
  }

  public findNomMarque(nom: String) : Observable<Object> {
    return this.http.get(`${this.apiServerUrl}/marquesname/${nom}`);
  }

  //ajout marque
  public ajoutMarque(info: Object): Observable<Object> {
    return this.http.post(`${this.apiServerUrl}/addmarque`, info);
  }
}
