import { TestBed } from '@angular/core/testing';

import { MarqueserviceService } from './marqueservice.service';

describe('MarqueserviceService', () => {
  let service: MarqueserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarqueserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
