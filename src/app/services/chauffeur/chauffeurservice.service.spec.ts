import { TestBed } from '@angular/core/testing';

import { ChauffeurserviceService } from './chauffeurservice.service';

describe('ChauffeurserviceService', () => {
  let service: ChauffeurserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChauffeurserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
