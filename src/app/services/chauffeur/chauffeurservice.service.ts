import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Chauffeur } from 'src/app/Model/chauffeur';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChauffeurserviceService {

  public formDatas !:  FormGroup; 


  private apiServerUrl = environment.apibaseUrl;
  constructor(private http: HttpClient) { }


  // nombre chauffeurs
 
  public totalchauffeur(): Observable<number> {
    return this.http.get<number>(`${this.apiServerUrl}/nbrechauffeur`);
  }
  //liste des chaufeur
  public getAllCahuffeurs(): Observable<Chauffeur[]> {
    return this.http.get<Chauffeur[]>(`${this.apiServerUrl}/chauffeurs`);
  }

   //ajout chauffeur
   public ajoutChauffeur(info: Object): Observable<Object> {
    return this.http.post(`${this.apiServerUrl}/addchauffeur`, info);
  }

   findByiduser(iduser :any ) {
    return this.http.get(`${this.apiServerUrl}/chauff/${iduser}`);
   
   } 

 

}
