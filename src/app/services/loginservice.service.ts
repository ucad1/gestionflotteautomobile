import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {

  islogin = false;
  chauffeur = false;
  client = false;
  mecanicien = false;
  admin = false;

  public formDatas !:  FormGroup;

  private apiServerUrl = environment.apibaseUrl;
  constructor(private http: HttpClient) { }


   //login chauff
   loginauth(mail :string,pwd : string ) {
    return this.http.get(`${this.apiServerUrl}/auth/${mail}`);
   
   } 

   upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.apiServerUrl}/upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }

  getFiles(): Observable<any> {
    return this.http.get(`${this.apiServerUrl}/files`);
  }

   
}
