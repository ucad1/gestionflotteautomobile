import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {

  private apiServerUrl = environment.apibaseUrl;
  constructor(private http: HttpClient) { }

  findByMail(mail :any ) {
    return this.http.get(`${this.apiServerUrl}/authadmin/${mail}`);
   
   } 
 
}
