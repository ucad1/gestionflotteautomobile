import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {MatTooltipModule} from '@angular/material/tooltip';
import { AdmindashbordComponent } from './admin/admindashbord/admindashbord.component';
import { AdminsidebarComponent } from './admin/adminsidebar/adminsidebar.component';
import { AdminnavbarComponent } from './admin/adminnavbar/adminnavbar.component';
import { StatistiquesComponent } from './admin/statistiques/statistiques.component';
import { NotusedComponent } from './admin/notused/notused.component';
import { ListemarquesComponent } from './marques/listemarques/listemarques.component';
import { AjoutmarquesComponent } from './marques/ajoutmarques/ajoutmarques.component';
import { AjoutvehiculeComponent } from './vehicule/ajoutvehicule/ajoutvehicule.component';
import { ListvehiculeComponent } from './vehicule/listvehicule/listvehicule.component';
import { AjoutchauffeurComponent } from './Personnels/Chaufeur/ajoutchauffeur/ajoutchauffeur.component';
import { ListechauffeurComponent } from './Personnels/Chaufeur/listechauffeur/listechauffeur.component';
import { ListemecanoComponent } from './Personnels/Mecanicien/listemecano/listemecano.component';
import { AjoutMecanoComponent } from './Personnels/Mecanicien/ajout-mecano/ajout-mecano.component';
import { GestionreservationComponent } from './admin/gestionreservation/gestionreservation.component';
import { AjoutmissionComponent } from './admin/Mission/ajoutmission/ajoutmission.component';
import { ListemissionComponent } from './admin/Mission/listemission/listemission.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { DashboardchauffeurComponent } from './DChauffeurs/dashboardchauffeur/dashboardchauffeur.component';
import { SidebarchauffeurComponent } from './DChauffeurs/sidebarchauffeur/sidebarchauffeur.component';
import { NavbarchauffeurComponent } from './DChauffeurs/navbarchauffeur/navbarchauffeur.component';
import { DeclarerpannesComponent } from './DChauffeurs/Alertes/declarerpannes/declarerpannes.component';
import { DeclareraccidentComponent } from './DChauffeurs/Alertes/declareraccident/declareraccident.component';
import { DemandeabsenceComponent } from './DChauffeurs/Alertes/demandeabsence/demandeabsence.component';
import { DeclareramendeComponent } from './DChauffeurs/Alertes/declareramende/declareramende.component';
import { VoirmissionsComponent } from './DChauffeurs/Missions/voirmissions/voirmissions.component';
import { DashboardmecanoComponent } from './DMecanicien/dashboardmecano/dashboardmecano.component';
import { SidebarmecanoComponent } from './DMecanicien/sidebarmecano/sidebarmecano.component';
import { NavbarmecanoComponent } from './DMecanicien/navbarmecano/navbarmecano.component';
import { ReparerpannesComponent } from './DMecanicien/Reparation/reparerpannes/reparerpannes.component';
import { ReparermaintenanceComponent } from './DMecanicien/Reparation/reparermaintenance/reparermaintenance.component';
import { DemandesabscencesComponent } from './DMecanicien/Absences/demandesabscences/demandesabscences.component';
import { ListdemandesabscencesComponent } from './DMecanicien/Absences/listdemandesabscences/listdemandesabscences.component';
import { AcceuilComponent } from './Home/acceuil/acceuil.component';
import { NavbaracceuilComponent } from  './Home/acceuil/navbaracceuil/navbaracceuil.component';
import { CarouselComponent } from './Home/acceuil/layout/carousel/carousel.component';
import { CardcomponentComponent } from './Home/acceuil/layout/cardcomponent/cardcomponent.component';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { HttpClientModule} from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {  MatButtonModule } from '@angular/material/button';
import { MesreservationsComponent } from './client/mesreservations/mesreservations.component';
import { HomeclientComponent } from './client/homeclient/homeclient.component';
import { NavbaracceuilclientComponent } from './client/navbaracceuilclient/navbaracceuilclient.component';
import { FormsModule }   from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { ImagecomponentComponent } from './imagecomponent/imagecomponent.component';
import { ModifiermarqueComponent } from './marques/modifiermarque/modifiermarque.component';
import { MatDialogModule,MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModifierVehiculeComponent } from './vehicule/modifier-vehicule/modifier-vehicule.component';
import { ModifierchauffeurComponent } from './Personnels/Chaufeur/modifierchauffeur/modifierchauffeur.component';
import { ModifierMecanoComponent } from './Personnels/Mecanicien/modifier-mecano/modifier-mecano.component';
import { LoginComponent } from './Auth/login/login.component';
import { SigninComponent } from './client/signin/signin.component';
import { UploadimgComponent } from './uploadimg/uploadimg.component';
import { CartclientComponent } from './client/cartclient/cartclient.component';

@NgModule({
  declarations: [
    AppComponent,
    AdmindashbordComponent,
    AdminsidebarComponent,
    AdminnavbarComponent,
    StatistiquesComponent,
    NotusedComponent,
    ListemarquesComponent,
    AjoutmarquesComponent,
    AjoutvehiculeComponent,
    ListvehiculeComponent,
    AjoutchauffeurComponent,
    ListechauffeurComponent,
    ListemecanoComponent,
    AjoutMecanoComponent,
    GestionreservationComponent,
    AjoutmissionComponent,
    ListemissionComponent,
    DashboardchauffeurComponent,
    SidebarchauffeurComponent,
    NavbarchauffeurComponent,
    DeclarerpannesComponent,
    DeclareraccidentComponent,
    DemandeabsenceComponent,
    DeclareramendeComponent,
    VoirmissionsComponent,
    DashboardmecanoComponent,
    SidebarmecanoComponent,
    NavbarmecanoComponent,
    ReparerpannesComponent,
    ReparermaintenanceComponent,
    DemandesabscencesComponent,
    ListdemandesabscencesComponent,
    AcceuilComponent,
    NavbaracceuilComponent,
    CarouselComponent,
    CardcomponentComponent,
    MesreservationsComponent,
    HomeclientComponent,
    NavbaracceuilclientComponent,
    ImagecomponentComponent,
    ModifiermarqueComponent,
    ModifierVehiculeComponent,
    ModifierchauffeurComponent,
    ModifierMecanoComponent,
    LoginComponent,
    SigninComponent,
    UploadimgComponent,
    CartclientComponent,
  
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatGridListModule,
    MatDialogModule,
        
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
