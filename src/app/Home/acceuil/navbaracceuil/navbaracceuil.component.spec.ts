import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbaracceuilComponent } from './navbaracceuil.component';

describe('NavbaracceuilComponent', () => {
  let component: NavbaracceuilComponent;
  let fixture: ComponentFixture<NavbaracceuilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbaracceuilComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NavbaracceuilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
