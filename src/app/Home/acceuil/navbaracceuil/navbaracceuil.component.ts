import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Chauffeur } from 'src/app/Model/chauffeur';
import { ChauffeurserviceService } from 'src/app/services/chauffeur/chauffeurservice.service';
import { ClientserviceService } from 'src/app/services/client/clientservice.service';
import { LoginserviceService } from 'src/app/services/loginservice.service';
import { MecanicienserviceService } from 'src/app/services/mecanicien/mecanicienservice.service';

@Component({
  selector: 'app-navbaracceuil',
  templateUrl: './navbaracceuil.component.html',
  styleUrls: ['./navbaracceuil.component.scss']
})
export class NavbaracceuilComponent implements OnInit {

  chauffeur: any={};
  mecanicien: any={};
  admin: any={};
  client : any={};
  utilisateur: any={};

  errorMessage !:string;
  name !: string;

  id!: number;
 
  loginForm !:  FormGroup;
  constructor(private router: Router,public fb: FormBuilder,
    public loginservice : LoginserviceService, public chauffeurservice : ChauffeurserviceService
    ,public clientservice: ClientserviceService, public mecanicienservice : MecanicienserviceService) { }

  ngOnInit(): void {
    this.loginservice.islogin = false;
    this.loginservice.chauffeur = false;
    this.loginservice.client = false;
    this.loginservice.admin = false;
    this.loginservice.mecanicien = false;


    this.infoForm();
  }

  infoForm(){
    this.loginForm = this.fb.group({
      mail : ['', Validators.required],
      password : ['', Validators.required]
    });
  }
  //login 
  login() {
    const val = this.loginForm.value ;
  
    this.loginservice.loginauth(val.mail, val.password).subscribe(
      res=>{
        this.utilisateur = res;
        if (this.utilisateur.password   == val.password ) {
          
          localStorage.setItem("iduser", this.utilisateur.iduser);
          localStorage.setItem("nom", this.utilisateur.nom);
          localStorage.setItem("prenom", this.utilisateur.prenom);
          localStorage.setItem("mail", this.utilisateur.mail);
          localStorage.setItem("password", this.utilisateur.password);
          localStorage.setItem("address", this.utilisateur.address);
          localStorage.setItem("photo", this.utilisateur.photo);
          
          localStorage.setItem("role", this.utilisateur.role);
          
          this.loginservice.islogin = true;
          if (this.utilisateur.role  == "CHAUFFEUR")
           {

          
           this.loginservice.chauffeur = true;
           
           this.chauffeurservice.findByiduser(localStorage.getItem('iduser')).subscribe({
            next :(response)=>{
              this.chauffeur= response;
              
           
          localStorage.setItem("numeropermis", this.chauffeur.numeropermis);
          localStorage.setItem("typepermis", this.chauffeur.typepermis);
          localStorage.setItem("datedelivrance", this.chauffeur.datedelivrance);
          localStorage.setItem("dateexpiration", this.chauffeur.dateexpiration);
          localStorage.setItem("numtel", this.chauffeur.numtel);
          
          localStorage.setItem("idchaufffeur", this.chauffeur.iduser);
          
          localStorage.setItem("status", this.chauffeur.status);
            this.router.navigate(['/chauffeurdash']);
            }
           })


        }else if (this.utilisateur.role  == "ADMIN") {
          localStorage.setItem("iduser", this.utilisateur.iduser);
          localStorage.setItem("nom", this.utilisateur.nom);
          localStorage.setItem("prenom", this.utilisateur.prenom);
          localStorage.setItem("mail", this.utilisateur.mail);
          localStorage.setItem("password", this.utilisateur.password);
          localStorage.setItem("address", this.utilisateur.address);
          localStorage.setItem("photo", this.utilisateur.photo);
          
          localStorage.setItem("numtel", this.admin.numtel);
          
          localStorage.setItem("idadmin", this.mecanicien.iduser);
          
        }else if(this.utilisateur.role  == "CLIENT") {

          this.clientservice.findByidusercli(localStorage.getItem('iduser')).subscribe({
            next: (res)=>{
              this.client = res;
              localStorage.setItem("numtel", this.client.numtel);
              
              localStorage.setItem("idclient", this.client.iduser);

              this.router.navigate(['/homeclient']);
              
            }
          })
         
        }else if(this.utilisateur.role  == "MECANICIEN") {
            this.mecanicienservice.findByidusermec(localStorage.getItem('iduser')).subscribe({
              next: (result)=>{
                this.mecanicien =result;
          
          localStorage.setItem("numtel", this.mecanicien.numtel);
          
          localStorage.setItem("idmecano", this.mecanicien.iduser);
        
        
          this.router.navigate(['/mecanodash']);
       
              }
            })
          }
        
        }else {
          alert("Mail ou Password incorrect!")
        }
       
      },
      error =>{
        

        alert( 'Login Incorrecte ')
      }


      
    );
  }


}
