import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminserviceService } from 'src/app/services/admin/adminservice.service';
import { LoginserviceService } from 'src/app/services/loginservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  chauffeur: any={};
  mecanicien: any={};
  admin: any={};
  client : any={};
  utilisateur: any={};

  errorMessage !:string;
  name !: string;
 
  loginForm !:  FormGroup;
  constructor(private router:Router,public fb: FormBuilder,
    public loginservice : LoginserviceService, public adminservice : AdminserviceService) { }

  ngOnInit(): void {

    this.loginservice.islogin = false;
    this.loginservice.chauffeur = false;
    this.loginservice.client = false;
    this.loginservice.admin = false;
    this.loginservice.mecanicien = false;


    this.loginForm = this.fb.group({
      mail : ['', Validators.required],
      password : ['', Validators.required]
    });
  }

  //login chauffeur
  login() {
    const val = this.loginForm.value ;
  
    this.loginservice.loginauth(val.mail, val.password).subscribe({
      next: (queryParams) => {
        this.utilisateur = queryParams;
        if (this.utilisateur.password   == val.password ) {
          
          localStorage.setItem("iduser", this.utilisateur.iduser);
          localStorage.setItem("nom", this.utilisateur.nom);
          localStorage.setItem("prenom", this.utilisateur.prenom);
          localStorage.setItem("mail", this.utilisateur.mail);
          localStorage.setItem("password", this.utilisateur.password);
          localStorage.setItem("address", this.utilisateur.address);
          localStorage.setItem("photo", this.utilisateur.photo);
          
          localStorage.setItem("role", this.utilisateur.role);
          
          this.loginservice.islogin = true;
          if (this.utilisateur.role  == "CHAUFFEUR")
           {
           this.loginservice.chauffeur = true;
           
          localStorage.setItem("numeropermis", this.chauffeur.numeropermis);
          localStorage.setItem("typepermis", this.chauffeur.typepermis);
          localStorage.setItem("datedelivrance", this.chauffeur.datedelivrance);
          localStorage.setItem("dateexpiration", this.chauffeur.dateexpiration);
          localStorage.setItem("numtel", this.chauffeur.numtel);
          
          localStorage.setItem("idchaufffeur", this.mecanicien.iduser);
          
          localStorage.setItem("status", this.chauffeur.status);
            this.router.navigate(['/chauffeurdash']);
        }else if (this.utilisateur.role  == "ADMIN") {
          localStorage.setItem("iduser", this.utilisateur.iduser);
          localStorage.setItem("nom", this.utilisateur.nom);
          localStorage.setItem("prenom", this.utilisateur.prenom);
          localStorage.setItem("mail", this.utilisateur.mail);
          localStorage.setItem("password", this.utilisateur.password);
          localStorage.setItem("address", this.utilisateur.address);
          localStorage.setItem("photo", this.utilisateur.photo);
          this.adminservice.findByMail(this.utilisateur.mail).subscribe({
            next : (res) => {
              this.admin = res;
              localStorage.setItem("numtel", this.admin.numtel);
          
              localStorage.setItem("idadmin", this.admin.idadmin);
              this.router.navigate(['/admin']);
            },
            error : (err : Error) =>  {
              err.message
            }
          })
          
      
        }else if(this.utilisateur.role  == "CLIENT") {
          localStorage.setItem("iduser", this.utilisateur.iduser);
          localStorage.setItem("nom", this.utilisateur.nom);
          localStorage.setItem("prenom", this.utilisateur.prenom);
          localStorage.setItem("mail", this.utilisateur.mail);
          localStorage.setItem("password", this.utilisateur.password);
          localStorage.setItem("address", this.utilisateur.address);
          localStorage.setItem("photo", this.utilisateur.photo);
          localStorage.setItem("numtel", this.client.numtel);
          
          localStorage.setItem("idclient", this.client.iduser);
         
          this.router.navigate(['/homeclient']);
          
        }else if(this.utilisateur.role  == "MECANICIEN") {
          
          localStorage.setItem("iduser", this.utilisateur.iduser);
          localStorage.setItem("nom", this.utilisateur.nom);
          localStorage.setItem("prenom", this.utilisateur.prenom);
          localStorage.setItem("mail", this.utilisateur.mail);
          localStorage.setItem("password", this.utilisateur.password);
          localStorage.setItem("address", this.utilisateur.address);
          localStorage.setItem("photo", this.utilisateur.photo);
          
          localStorage.setItem("numtel", this.mecanicien.numtel);
          
          localStorage.setItem("idmecano", this.mecanicien.iduser);
        }
        
        }else {
          alert("Mail ou Password incorrect!")
        }
       
      },
      error: (err: any) => {
    
        alert( 'Login Incorrecte ')
      }
    }


      
    );
  }


  
 


}
