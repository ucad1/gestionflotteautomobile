import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Client } from 'src/app/Model/client';
import { ClientserviceService } from 'src/app/services/client/clientservice.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  client !: Client;
  constructor(public clientservice: ClientserviceService,private fb : FormBuilder, private route: Router) { }

  ngOnInit(): void {
    this.infoForm();
  }

  //init
  infoForm() {
    this.clientservice.formDatas = this.fb.group({
      id : null,
      nom : ['' , Validators.required] ,
      prenom : ['' , Validators.required] ,
      address : ['' , Validators.required] ,
      mail : ['' , Validators.required] ,
      numtel : ['' , Validators.required] ,
      password : ['' , Validators.required] ,
      
      cpassword : ['' , Validators.required] ,
    })
  }

  inscrire() {
    if(this.clientservice.formDatas.valid){
      const val = this.clientservice.formDatas.value ;
  
      if(val.password == val.cpassword){
        this.clientservice.inscrireclient(this.clientservice.formDatas.value).subscribe({
          next : (res) =>{
            res = this.client;
            alert("cInscription reussii!!!!!");
            this.clientservice.formDatas.reset();
            this.route.navigate(['/home']);
          }, 
          error : (err : HttpErrorResponse) =>{
            err.message
  
          }
        })
      }else {
         alert("les mots de pass ne correspond pas");
      }
    }
  }

 
}
