import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbaracceuilclientComponent } from './navbaracceuilclient.component';

describe('NavbaracceuilclientComponent', () => {
  let component: NavbaracceuilclientComponent;
  let fixture: ComponentFixture<NavbaracceuilclientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbaracceuilclientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NavbaracceuilclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
