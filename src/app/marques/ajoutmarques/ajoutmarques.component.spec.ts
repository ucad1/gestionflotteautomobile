import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutmarquesComponent } from './ajoutmarques.component';

describe('AjoutmarquesComponent', () => {
  let component: AjoutmarquesComponent;
  let fixture: ComponentFixture<AjoutmarquesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutmarquesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AjoutmarquesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
