import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Marque } from 'src/app/Model/marque';
import { MarqueserviceService } from 'src/app/services/marque/marqueservice.service';

@Component({
  selector: 'app-ajoutmarques',
  templateUrl: './ajoutmarques.component.html',
  styleUrls: ['./ajoutmarques.component.scss']
})
export class AjoutmarquesComponent implements OnInit {


  marque !: Marque;

  //myDate :Date = new Date();

  constructor( private fb : FormBuilder,public marqueservice : MarqueserviceService, private route : Router) {
 
  }

  ngOnInit(): void {

    //initialisation
    this.infoForm();
  }
  
  infoForm() {
    this.marqueservice.formDatas = this.fb.group(
      {
       id : null,
       nommarque : ['' , Validators.required] ,
       creationDate : ['' , Validators.required] ,
       updationDate : ['' , Validators.required] ,
      }
     )
  }
  resetForm() {
    this.marqueservice.formDatas.reset();
  }
  //Ajout marque
  addMarque() {
    if(this.marqueservice.formDatas.valid){

      this.marqueservice.ajoutMarque(this.marqueservice.formDatas.value).subscribe({
        next : (res) => {
          res = this.marque
          alert("Marque ajouter avec success !");
          this.marqueservice.formDatas.reset();
          this.route.navigate(['/listmarque']);

        },
        error : (err : HttpErrorResponse) => {

          alert("Echec.............!")
          err.message
        }
      })
    }
  }


}
