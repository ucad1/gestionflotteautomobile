import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit ,AfterViewInit, ViewChild} from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Marque } from 'src/app/Model/marque';
import { MarqueserviceService } from 'src/app/services/marque/marqueservice.service';
import { ModifiermarqueComponent } from '../modifiermarque/modifiermarque.component';



@Component({
  selector: 'app-listemarques',
  templateUrl: './listemarques.component.html',
  styleUrls: ['./listemarques.component.scss']
})
export class ListemarquesComponent implements OnInit {

  
  public marques !: Marque[];

  displayedColumns: string[] = ['id','nommarque','creationDate','updationDate','action'];
  dataSource!: MatTableDataSource<Marque>;

  @ViewChild(MatPaginator) paginator!: MatPaginator ;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private marqueservice: MarqueserviceService, public dialogRef: MatDialog) {

   
  }
  ngOnInit(): void {

    this.getAllMarques();

  }

  //modifier marques
  updateMarque() {
    //this.crudApi.choixmenu = "A";
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width = "70%";
    //dialogConfig.data="gdddd";
    this.dialogRef.open(ModifiermarqueComponent, {
      width: '60%'
    })
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

   
  }
 

  getAllMarques() {

    this.marqueservice.getAll().subscribe(
          {
            next: (res) =>{

              this.dataSource = new MatTableDataSource(res);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;

            },
            error: (err: HttpErrorResponse)=> {
              
              err.message
            }
          }
      );
    
   
  }


}


function next(next: any, arg1: (res: any) => void, error: any, arg3: (err: HttpErrorResponse) => void) {
  throw new Error('Function not implemented.');
}

