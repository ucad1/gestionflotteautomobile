import { Component, OnInit } from '@angular/core';
import { MarqueserviceService } from 'src/app/services/marque/marqueservice.service';

@Component({
  selector: 'app-modifiermarque',
  templateUrl: './modifiermarque.component.html',
  styleUrls: ['./modifiermarque.component.scss']
})
export class ModifiermarqueComponent implements OnInit {

  constructor(public marqueservice : MarqueserviceService) { }

  ngOnInit(): void {
  }

}
