import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Mecanicien } from 'src/app/Model/mecanicien';
import { MecanicienserviceService } from 'src/app/services/mecanicien/mecanicienservice.service';

@Component({
  selector: 'app-listemecano',
  templateUrl: './listemecano.component.html',
  styleUrls: ['./listemecano.component.scss']
})
export class ListemecanoComponent implements OnInit {

  displayedColumns: string[] = ['id','nom','prenom','mail','numtel','address','action'];
  dataSource!: MatTableDataSource<Mecanicien>;

  @ViewChild(MatPaginator) paginator!: MatPaginator ;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(private mecanicienservice : MecanicienserviceService, public route: Router) { }

  ngOnInit(): void {

    this.getAllMecanos();
  }

  updatemecano() {
    this.route.navigate(['/modifermecano'])
  }
  //
  getAllMecanos() {

    this.mecanicienservice.getAllMecano().subscribe(
          {
            next: (res) =>{

              
              this.dataSource = new MatTableDataSource(res);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;

            },
           error: (err: HttpErrorResponse)=> {
              err.message
            }
          }
      );
    }
  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

   
  }

}
