import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListemecanoComponent } from './listemecano.component';

describe('ListemecanoComponent', () => {
  let component: ListemecanoComponent;
  let fixture: ComponentFixture<ListemecanoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListemecanoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListemecanoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
