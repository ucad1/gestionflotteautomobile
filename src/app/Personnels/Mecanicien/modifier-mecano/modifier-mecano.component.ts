import { Component, OnInit } from '@angular/core';
import { MecanicienserviceService } from 'src/app/services/mecanicien/mecanicienservice.service';

@Component({
  selector: 'app-modifier-mecano',
  templateUrl: './modifier-mecano.component.html',
  styleUrls: ['./modifier-mecano.component.scss']
})
export class ModifierMecanoComponent implements OnInit {

  constructor(public mecanicienservice: MecanicienserviceService) { }

  ngOnInit(): void {
  }

}
