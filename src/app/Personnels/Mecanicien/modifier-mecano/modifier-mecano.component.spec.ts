import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierMecanoComponent } from './modifier-mecano.component';

describe('ModifierMecanoComponent', () => {
  let component: ModifierMecanoComponent;
  let fixture: ComponentFixture<ModifierMecanoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifierMecanoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModifierMecanoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
