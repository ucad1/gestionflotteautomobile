import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutMecanoComponent } from './ajout-mecano.component';

describe('AjoutMecanoComponent', () => {
  let component: AjoutMecanoComponent;
  let fixture: ComponentFixture<AjoutMecanoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutMecanoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AjoutMecanoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
