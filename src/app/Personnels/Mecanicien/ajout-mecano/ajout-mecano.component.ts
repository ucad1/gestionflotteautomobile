import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Mecanicien } from 'src/app/Model/mecanicien';
import { MecanicienserviceService } from 'src/app/services/mecanicien/mecanicienservice.service';

@Component({
  selector: 'app-ajout-mecano',
  templateUrl: './ajout-mecano.component.html',
  styleUrls: ['./ajout-mecano.component.scss']
})
export class AjoutMecanoComponent implements OnInit {

  mecanicien !: Mecanicien;
  constructor(public mecanicienservice : MecanicienserviceService, private fb : FormBuilder, private route: Router) { }

  ngOnInit(): void {
    this.infoForm();
  }
  
  infoForm() {
    this.mecanicienservice.formdata = this.fb.group({
      id : null,
      nom : ['' , Validators.required] ,
      prenom : ['' , Validators.required] ,
      address : ['' , Validators.required] ,
      mail : ['' , Validators.required] ,
      numtel : ['' , Validators.required] ,
      password : ['' , Validators.required] ,
    })
  }

  resetForm(){
    this.mecanicienservice.formdata.reset();
  }


  ajoutMecanicien(){
    if(this.mecanicienservice.formdata.valid){
      this.mecanicienservice.ajoutMecano(this.mecanicienservice.formdata.value).subscribe({
        next : (res) =>{
          res = this.mecanicien;
          alert("mecanicien ajouter avec success !");
          this.mecanicienservice.formdata.reset();
          this.route.navigate(['/listemecano']);
        }, 
        error : (err : HttpErrorResponse) =>{
          err.message

        }
      })
    }
  }

}
