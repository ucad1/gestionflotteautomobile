import { Component, OnInit } from '@angular/core';
import { ChauffeurserviceService } from 'src/app/services/chauffeur/chauffeurservice.service';

@Component({
  selector: 'app-modifierchauffeur',
  templateUrl: './modifierchauffeur.component.html',
  styleUrls: ['./modifierchauffeur.component.scss']
})
export class ModifierchauffeurComponent implements OnInit {

  constructor(public chauffeurservice: ChauffeurserviceService) { }

  ngOnInit(): void {
  }

}
