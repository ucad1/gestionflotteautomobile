import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierchauffeurComponent } from './modifierchauffeur.component';

describe('ModifierchauffeurComponent', () => {
  let component: ModifierchauffeurComponent;
  let fixture: ComponentFixture<ModifierchauffeurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifierchauffeurComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModifierchauffeurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
