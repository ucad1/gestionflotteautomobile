import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Chauffeur } from 'src/app/Model/chauffeur';
import { ChauffeurserviceService } from 'src/app/services/chauffeur/chauffeurservice.service';

@Component({
  selector: 'app-listechauffeur',
  templateUrl: './listechauffeur.component.html',
  styleUrls: ['./listechauffeur.component.scss']
})
export class ListechauffeurComponent implements OnInit {

  dColumns: string[] = ['id','nom','prenom','mail','numtel','numeropermis','typepermis','datedelivrance','dateexpiration','action'];
  dataSrce!: MatTableDataSource<Chauffeur>;

 @ViewChild(MatPaginator) paginator!: MatPaginator ;
 @ViewChild(MatSort) sort!: MatSort;

 dates = new Date();

  constructor(private chauffeurservice : ChauffeurserviceService,public route: Router) { }

  ngOnInit(): void {

    this.getAllChauffeur();
  }


  updateChauffeur(){
    this.route.navigate(['/modifchauffeur'])
  }
  //la liste des chauffeurs
  getAllChauffeur() {

    this.chauffeurservice.getAllCahuffeurs().subscribe({
       next : (res) =>{
         this.dataSrce = new MatTableDataSource(res);
         this.dataSrce.paginator = this.paginator;
         this.dataSrce.sort = this.sort;
       },
       error : (err : HttpErrorResponse) =>{
         err.message
       }
    });

     }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSrce.filter = filterValue.trim().toLowerCase();

   
  }



}
