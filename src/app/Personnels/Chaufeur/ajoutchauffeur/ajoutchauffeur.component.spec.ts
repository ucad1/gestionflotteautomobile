import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutchauffeurComponent } from './ajoutchauffeur.component';

describe('AjoutchauffeurComponent', () => {
  let component: AjoutchauffeurComponent;
  let fixture: ComponentFixture<AjoutchauffeurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjoutchauffeurComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AjoutchauffeurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
