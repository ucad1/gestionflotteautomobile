import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Chauffeur } from 'src/app/Model/chauffeur';
import { ChauffeurserviceService } from 'src/app/services/chauffeur/chauffeurservice.service';

@Component({
  selector: 'app-ajoutchauffeur',
  templateUrl: './ajoutchauffeur.component.html',
  styleUrls: ['./ajoutchauffeur.component.scss']
})
export class AjoutchauffeurComponent implements OnInit {
  
  chauffeur !: Chauffeur;

  constructor(public chauffeurservice : ChauffeurserviceService,private fb :FormBuilder , private route : Router) { }

  ngOnInit(): void {

    this.infoForm();
  }

  
  infoForm() {
    this.chauffeurservice.formDatas = this.fb.group(
      {
       id : null,
       nom : ['' , Validators.required] ,
       prenom : ['' , Validators.required] ,
       numeropermis : ['' , Validators.required] ,
       typepermis : ['' , Validators.required] ,
       datedelivrance : ['' , Validators.required] ,
       dateexpiration : ['' , Validators.required] ,
       mail : ['' , Validators.required] ,
       numtel : ['' , Validators.required] ,
       address : ['' , Validators.required] ,
       password : ['' , Validators.required] ,
      }
     )
  }


  resetForm() {
    this.chauffeurservice.formDatas.reset();
  }

  //ajout chauffeur
  ajoutchauffeur() {
    if(this.chauffeurservice.formDatas.valid) {
      this.chauffeurservice.ajoutChauffeur(this.chauffeurservice.formDatas.value).subscribe({
        next: (res)=> {
          res = this.chauffeur;
          alert("chauffeur ajouter avec success !");
          this.chauffeurservice.formDatas.reset();
          this.route.navigate(['/listechauffeur']);

        },
        error : (err : HttpErrorResponse)=>{
          err.message
        }
      })
    }
  }

}
